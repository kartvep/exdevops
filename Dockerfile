FROM python:3.8-slim-buster

COPY --chown=root:root requirements/main.txt /app/requirements.txt
COPY --chown=root:root wait_for.sh /usr/local/bin
COPY --chown=root:root slow_start.sh /usr/local/bin
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
      netcat \
      sqlite3 \
    && \
    apt-get clean && \
    pip install -r /app/requirements.txt && \
    pip install gunicorn
COPY --chown=root:root manage.py /app/manage.py
COPY --chown=root:root src /app/src
COPY --chown=root:root configs /app/configs
COPY --chown=root:root README.md /app/README.md
RUN chmod a-w -R /app

ENV APP_CONFIG_PATH=/app/configs/prod.env
ENV WORKERS=4
ARG version
ENV APP_VERSION=$version

EXPOSE 8000
WORKDIR /app

CMD gunicorn src.wsgi \
    --workers $WORKERS \
    --user www-data \
    --group www-data \
    --bind [::]:8000
