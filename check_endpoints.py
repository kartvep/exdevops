import time

import requests


def test_ping():
    resp = requests.get('http://docker:8000/-/ping')
    if resp.status_code != 200:
        print(resp.text)
    assert resp.status_code == 200


def test_status():
    resp = requests.get('http://docker:8000/-/status')
    if resp.status_code != 200:
        print(resp.text)
    assert resp.status_code == 200


def test_metrics():
    resp = requests.get('http://docker:8000/-/metrics')
    assert resp.status_code == 200


def test_root():
    resp = requests.get('http://docker:8000/')
    assert resp.status_code == 200

def test_performance():
    tests = [
        (test_ping, 100, 1),
        (test_status, 30, 1),
        (test_metrics, 30, 1),
        (test_root, 100, 1),
    ]

    for test, num, limit in tests:
        start = time.time()
        for _ in range(num):
            test()
        assert time.time() - start < limit
