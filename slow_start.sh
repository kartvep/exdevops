#!/bin/sh
wait_for.sh db:5432 -- /app/manage.py migrate
gunicorn src.wsgi --workers $WORKERS --user www-data --group www-data --bind [::]:8000
